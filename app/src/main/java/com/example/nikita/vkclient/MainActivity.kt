package com.example.nikita.vkclient

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import com.vk.sdk.util.VKUtil

class MainActivity : AppCompatActivity() {

    private val scope : Array<String> = arrayOf(VKScope.MESSAGES, VKScope.FRIENDS)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        VKSdk.login(this, *scope)

        /*val fingerprints = VKUtil.getCertificateFingerprint(this, packageName)
        for (fingerprint in fingerprints){
            Log.d("vktag", fingerprint)
        }*/
    }
}
